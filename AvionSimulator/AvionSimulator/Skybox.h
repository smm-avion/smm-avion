#pragma once

#include <GL/glew.h>
#include <iostream>
#include <vector>
#include <glm.hpp>
#include <glfw3.h>
#include "Shader.h"
#include "Camera.h"

class Skybox
{
public:
	unsigned int loadCubemap(std::vector<std::string> faces);
	void skyboxInit();
	void renderSkybox(Shader& shader, Camera& camera);
	void destroySkybox();
	void changeTime();

protected:
	bool isDay = true;
	unsigned int cubemapTexture;
	unsigned int skyboxVAO, skyboxVBO;
	float skyboxVertices[110] = {
		// positions
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f
	};
	std::vector<std::string> faces{
			"Assets/Skybox/skyboxDay-right.jpg",
			"Assets/Skybox/skyboxDay-left.jpg",
			"Assets/Skybox/skyboxDay-top.jpg",
			"Assets/Skybox/skyboxDay-bottom.jpg",
			"Assets/Skybox/skyboxDay-front.jpg",
			"Assets/Skybox/skyboxDay-back.jpg"
	};
};

