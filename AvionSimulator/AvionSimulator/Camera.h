#pragma once
#include <glfw3.h>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>

enum ECameraMovementType
{
	UNKNOWN,
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN,
	TEST // TO DO: de eliminat mai tarziu
};

class Camera
{
private:
	// Default camera values
	const float zNEAR = 0.1f;
	const float zFAR = 50000.f;
	const float YAW = -90.0f;
	const float PITCH = 0.0f;
	const float FOV = 45.0f;
	glm::vec3 startPosition;

public:
	Camera(const int width, const int height, const glm::vec3& position);

	void Set(const int width, const int height, const glm::vec3& position);

	void Reset(const int width, const int height);

	void Reshape(int windowWidth, int windowHeight);

	const glm::mat4 GetViewMatrix() const;

	const glm::vec3 GetPosition() const;

	const glm::mat4 GetProjectionMatrix() const;

	void ProcessKeyboard(ECameraMovementType direction, float deltaTime);

	void MouseControl(float xPos, float yPos);
	void MouseControlCircle(float xPos, float yPos);
	void ProcessMouseScroll(float yOffset);
	void RotateAroundAPoint(float angle);
	void SetPosition(glm::vec3 pos) {
		position = pos;
	}
	float GetLastX() {
		return lastX;
	}
	float GetLastY() {
		return lastY;
	}

private:
	void ProcessMouseMovement(float xOffset, float yOffset, bool constrainPitch = true);



protected:
	const float cameraSpeedFactor = 5000.0f; // TO DO: de pus inapoi la 2.5 sau cv mai mic
	const float mouseSensitivity = 0.1f;

	// Perspective properties
	float zNear;
	float zFar;
	float FoVy;
	int width;
	int height;
	bool isPerspective;

	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 right;
	glm::vec3 up;
	glm::vec3 worldUp;

	// Euler Angles
	float yaw;
	float pitch;

	bool bFirstMouseMove = true;
	float lastX = 0.f, lastY = 0.f;

public:
	void UpdateCameraVectors();

	float GetYaw() {
		return yaw;
	}
	float GetPitch() {
		return pitch;
	}
	void SetYaw(float pass) {
		yaw=pass;
		
	}
	void SetPitch(float pass) {
		pitch = pass;
	
	}
	glm::vec3 GetForward() {
		return forward;
	}
	 void SetForward(glm::vec3 vec) {
		 forward = vec;
	}
	float GetCameraSpeedFactor() {
		return cameraSpeedFactor;
	}
};

