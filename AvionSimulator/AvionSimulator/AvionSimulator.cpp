#include <stdlib.h> // necesare pentru citirea shader-elor
#include <stdio.h>
#include <math.h> 

#include <GL/glew.h>

#include <GLM.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <glfw3.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include "Camera.h"
#include "Shader.h"
#include "Skybox.h"
#include "Model.h"

#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

Camera* pCamera = nullptr;
glm::vec3 planeMovement = glm::vec3(0.0f, 0.0f, 0.0f);
Skybox skybox;

void Cleanup()
{
	delete pCamera;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow* window);

// timing
double deltaTime = 0.0f;	// time between current frame and last frame
double lastFrame = 0.0f;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
}

float turnHorizontal = 0;
float turnVertical = 0;
float planeYaw = 0;
float camYaw = 0;
float planePitch = 0;
float camPitch = 0;
float angle = 0;
glm::vec3  center = glm::vec3(0);
glm::vec3 planePosition = glm::vec3(0);
glm::vec3 headPosition = glm::vec3(0);
glm::vec3 planeForward = glm::vec3(0);
float speed = 0;
int main()
{
	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation -- aici atribui marimea windowului, si titlul aplicatiei
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Avion simulare", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback); // setarea functiei mouse_callback
	glfwSetScrollCallback(window, scroll_callback); //setarea functiei scroll_callback
	glfwSetKeyCallback(window, key_callback); //setarea functiei key_callback

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // de decomentat daca vrei sa nu mai ai voie sa misti mouse-ul in afara windowului

	glewInit();

	glEnable(GL_DEPTH_TEST);

	// Create camera
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(-13616.1, 2374.27, 8224.27));
	planeYaw = pCamera->GetYaw();
	camYaw = pCamera->GetYaw();
	planePitch = pCamera->GetPitch();
	camPitch = pCamera->GetPitch();
	//Create skybox
	skybox.skyboxInit();
	Shader skyboxShader("Shaders/skybox.vs", "Shaders/skybox.fs");
	skyboxShader.use();
	skyboxShader.setInt("skybox", 0);
	
	//Load terrain
	Shader terrainShader("Shaders/model_loading.vs", "Shaders/model_loading.fs");
	Model terrainModel("Assets/Map/terrain.obj");

	//Load plane
	Shader planeShader("Shaders/model_loading.vs", "Shaders/model_loading.fs");
	Model planeModel("Assets/Plane/DorandAR1.obj");

	//load empty hangar
	Shader hangarShader1("Shaders/model_loading.vs", "Shaders/model_loading.fs");
	Model hangarModel1("Assets/Hangar/Hangar.obj");

	//load hangar with planes
	Shader hangarShader2("Shaders/model_loading.vs", "Shaders/model_loading.fs");
	Model hangarModel2("Assets/Hangar/Hangar.obj");
	Shader staticPlaneShader("Shaders/model_loading.vs", "Shaders/model_loading.fs");
	Model staticPlaneModel("Assets/Plane/DorandAR1.obj");
	Shader staticPlaneShader2("Shaders/model_loading.vs", "Shaders/model_loading.fs");
	Model staticPlaneModel2("Assets/Plane/DorandAR1.obj");

	//load control tower
	Shader towerShader("Shaders/model_loading.vs", "Shaders/model_loading.fs");
	Model towerModel("Assets/ControlTower/Tower_Control.obj");

	//load runaway
	Shader runawayShader("Shaders/model_loading.vs", "Shaders/model_loading.fs");
	Model runawayModel("Assets/Runaway/Runaway.obj");
	center = pCamera->GetPosition();// +glm::vec3(10.0f, 0.0f, 0.0f);
	
	planePosition = pCamera->GetPosition();// +glm::vec3(0.0f, 0.0f, -10.0f);
	headPosition = planePosition + glm::vec3(0.0f, -2.0f, -2.0f);
	planeForward = pCamera->GetForward();
	// render loop
	while (!glfwWindowShouldClose(window)) {
		// per-frame time logic
		double currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		processInput(window);

		//window background
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//render model
		// don't forget to enable shader before setting uniforms
		planeShader.use();
		terrainShader.use();
		hangarShader1.use();
		hangarShader2.use();
		staticPlaneShader.use();
		staticPlaneShader2.use();
		towerShader.use();
		runawayShader.use();

		// view/projection transformations

		glm::mat4 projection = pCamera->GetProjectionMatrix();
		glm::mat4 view = pCamera->GetViewMatrix();

		terrainShader.setMat4("projection", projection);
		terrainShader.setMat4("view", view);

		planeShader.setMat4("projection", projection);
		planeShader.setMat4("view", view);

		hangarShader1.setMat4("projection", projection);
		hangarShader1.setMat4("view", view);

		hangarShader2.setMat4("projection", projection);
		hangarShader2.setMat4("view", view);

		staticPlaneShader.setMat4("projection", projection);
		staticPlaneShader.setMat4("view", view);

		staticPlaneShader2.setMat4("projection", projection);
		staticPlaneShader2.setMat4("view", view);

		towerShader.setMat4("projection", projection);
		towerShader.setMat4("view", view);

		runawayShader.setMat4("projection", projection);
		runawayShader.setMat4("view", view);

		
		glm::mat4 model = glm::mat4(1.0f);
		
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		
		
		glm::mat4 modelPlane = glm::mat4(1.0f);
		

		

		modelPlane = glm::translate(modelPlane, pCamera->GetPosition()+pCamera->GetForward()*13.f);
		modelPlane = glm::translate(modelPlane, glm::vec3(0.0f, -1.5f, 0.0f));
		
		modelPlane = glm::rotate(modelPlane, glm::radians(-planeYaw+180), glm::vec3(0.0f, 1.0f, 0.0f));
		modelPlane = glm::rotate(modelPlane, glm::radians(-planePitch), glm::vec3(0.0f, 0.0f, 1.0f));

		modelPlane = glm::rotate(modelPlane, glm::radians(turnVertical), glm::vec3(0.0f, 0.0f, 1.0f));
		modelPlane = glm::rotate(modelPlane, glm::radians(turnHorizontal), glm::vec3(1.0f, 0.0f, 0.0f));
	
		modelPlane = glm::translate(modelPlane, glm::vec3(0.0f, 0.0f, -3.7f));

		glm::mat4 modelHangar1 = glm::mat4(1.0f);
		modelHangar1 = glm::translate(modelHangar1, glm::vec3(-11153.5, 2075.56, 1961.32));
		modelHangar1 = glm::scale(modelHangar1, glm::vec3(50.0f, 50.0f, 50.0f));
		modelHangar1 = glm::rotate(modelHangar1, 1.57f, glm::vec3(0.f, 1.f, 0.f));

		glm::mat4 modelHangar2 = glm::mat4(1.0f);
		modelHangar2 = glm::translate(modelHangar2, glm::vec3(-11153.5, 2075.56, 5961.32));
		modelHangar2 = glm::scale(modelHangar2, glm::vec3(50.0f, 50.0f, 50.0f));
		modelHangar2 = glm::rotate(modelHangar2, 1.57f, glm::vec3(0.f, 1.f, 0.f));

		glm::mat4 modelPlaneStatic = glm::mat4(1.0f);
		modelPlaneStatic = glm::translate(modelPlaneStatic, glm::vec3(-10853.5, 2175.56, 5961.32));  
		modelPlaneStatic = glm::scale(modelPlaneStatic, glm::vec3(70.0f, 70.0f, 70.0f));

		glm::mat4 modelPlaneStatic2 = glm::mat4(1.0f);
		modelPlaneStatic2 = glm::translate(modelPlaneStatic2, glm::vec3(-11553.5, 2175.56, 5561.32));
		modelPlaneStatic2 = glm::scale(modelPlaneStatic2, glm::vec3(70.0f, 70.0f, 70.0f));

		glm::mat4 modelTower = glm::mat4(1.0f);
		modelTower = glm::translate(modelTower, glm::vec3(-11081.4, 2156.34, 8099.35));
		modelTower = glm::scale(modelTower, glm::vec3(60.0f, 60.0f, 60.0f));

		glm::mat4 modelRunaway = glm::mat4(1.0f);
		modelRunaway = glm::translate(modelRunaway, glm::vec3(-12673.8, 2000, 4469.65));
		modelRunaway = glm::scale(modelRunaway, glm::vec3(1.0f, 1.0f, 1.0f));
		modelRunaway = glm::rotate(modelRunaway, 4.71239f, glm::vec3(1.f, 0.f, 0.f));
		modelRunaway = glm::rotate(modelRunaway, 1.57f, glm::vec3(0.f, 0.f, 1.f));

		terrainShader.setMat4("model", model);
		terrainModel.Draw(terrainShader);

		planeShader.setMat4("model", modelPlane);
		planeModel.Draw(planeShader);

		hangarShader1.setMat4("model", modelHangar1);
		hangarModel1.Draw(hangarShader1);

		hangarShader2.setMat4("model", modelHangar2);
		hangarModel2.Draw(hangarShader2);

		staticPlaneShader.setMat4("model", modelPlaneStatic);
		staticPlaneModel.Draw(staticPlaneShader);

		staticPlaneShader2.setMat4("model", modelPlaneStatic2);
		staticPlaneModel2.Draw(staticPlaneShader2);

		towerShader.setMat4("model", modelTower);
		towerModel.Draw(towerShader);

		runawayShader.setMat4("model", modelRunaway);
		runawayModel.Draw(runawayShader);

		//render the skybox
		glDepthFunc(GL_LEQUAL);
		skyboxShader.use();
		skybox.renderSkybox(skyboxShader, *pCamera);

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	Cleanup();

	// glfw: terminate, clearing all previously allocated GLFW resources
	glfwTerminate();
	//deallocate skybox
	skybox.destroySkybox();
	return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow* window)
{
	
	float velocity = (float)(pCamera->GetCameraSpeedFactor() * deltaTime);
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		skybox.destroySkybox();
		skybox.changeTime();
		skybox.skyboxInit();
	}

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{

		//pCamera->MouseControl(pCamera->GetLastX() - 1, pCamera->GetLastY());
		planeYaw -=0.3;
		camYaw -= 0.3;
		planeForward.x = cos(glm::radians(planeYaw)) * cos(glm::radians(planePitch));
		planeForward.y = sin(glm::radians(planePitch));
		planeForward.z = sin(glm::radians(planeYaw)) * cos(glm::radians(planePitch));
		planeForward = glm::normalize(planeForward);
		glm::vec3 camForward;
		camForward.x = cos(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward.y = sin(glm::radians(camPitch));
		camForward.z = sin(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward = glm::normalize(camForward);
		pCamera->SetForward(camForward);

	
		if (turnHorizontal < 45) {
			turnHorizontal++;
		}
	}
	else {
		
		if (turnHorizontal > 0) {
			turnHorizontal--;
		}
	}

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		//pCamera->MouseControl(pCamera->GetLastX() + 1, pCamera->GetLastY());
		planeYaw+=0.3;
		camYaw += 0.3;
		planeForward.x = cos(glm::radians(planeYaw)) * cos(glm::radians(planePitch));
		planeForward.y = sin(glm::radians(planePitch));
		planeForward.z = sin(glm::radians(planeYaw)) * cos(glm::radians(planePitch));
		planeForward = glm::normalize(planeForward);
		glm::vec3 camForward;
		camForward.x = cos(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward.y = sin(glm::radians(camPitch));
		camForward.z = sin(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward = glm::normalize(camForward);
		pCamera->SetForward(camForward);

		
		if (turnHorizontal > -45) {
			turnHorizontal--;
		}
	}
	else {
	
		if (turnHorizontal < 0) {
			turnHorizontal++;
		}
	}

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{

		//pCamera->MouseControl(pCamera->GetLastX(), pCamera->GetLastY() - 0.5);
		//pCamera->MouseControl(pCamera->GetLastX() - 1, pCamera->GetLastY());
		planePitch+=0.3;
		camPitch += 0.3;
		planeForward.x = cos(glm::radians(planeYaw)) * cos(glm::radians(planePitch));
		planeForward.y = sin(glm::radians(planePitch));
		planeForward.z = sin(glm::radians(planeYaw)) * cos(glm::radians(planePitch));
		planeForward = glm::normalize(planeForward);
		glm::vec3 camForward;
		camForward.x = cos(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward.y = sin(glm::radians(camPitch));
		camForward.z = sin(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward = glm::normalize(camForward);
		pCamera->SetForward(camForward);
		
		if (turnVertical > -15) {
			turnVertical--;
		}
	}
	else {
		if (turnVertical < 0) {
			turnVertical++;
		}
		
		
	}

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		//pCamera->MouseControl(pCamera->GetLastX(), pCamera->GetLastY() + 0.5);
		planePitch -=0.3 ;
		camPitch -= 0.3;
		planeForward.x = cos(glm::radians(planeYaw)) * cos(glm::radians(planePitch));
		planeForward.y = sin(glm::radians(planePitch));
		planeForward.z = sin(glm::radians(planeYaw)) * cos(glm::radians(planePitch));
		planeForward = glm::normalize(planeForward);
		glm::vec3 camForward;
		camForward.x = cos(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward.y = sin(glm::radians(camPitch));
		camForward.z = sin(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward = glm::normalize(camForward);
		pCamera->SetForward(camForward);
	

		if (turnVertical < 20) {
			turnVertical++;
		}
	}
	else {
		if (turnVertical > 0) {
			turnVertical--;
		}
		
		
	}


	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
		//pCamera->ProcessKeyboard(FORWARD, (float)deltaTime);
		if (speed < 100) {
			speed+=0.1;
		}
		else {
			speed-=0.1;
		}
		
	}
	planePosition += planeForward * speed;
	pCamera->SetPosition(pCamera->GetPosition() + planeForward* speed);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
		//pCamera->ProcessKeyboard(BACKWARD, (float)deltaTime);
		if (speed>0) {
			speed--;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		//pCamera->ProcessKeyboard(LEFT, (float)deltaTime);
		//pCamera->MouseControl(pCamera->GetLastX() - 1, pCamera->GetLastY());
		camYaw += 0.5;
		
		
		glm::vec3 camForward;
		camForward.x = cos(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward.y = sin(glm::radians(camPitch));
		camForward.z = sin(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward = glm::normalize(camForward);
		pCamera->SetForward(camForward);

		//pCamera->SetForward(planeForward);
		
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		//pCamera->ProcessKeyboard(RIGHT, (float)deltaTime);
		//pCamera->MouseControl(pCamera->GetLastX() - 5, pCamera->GetLastY());
		camYaw -= 0.5;
	
		glm::vec3 camForward;
		camForward.x = cos(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward.y = sin(glm::radians(camPitch));
		camForward.z = sin(glm::radians(camYaw)) * cos(glm::radians(camPitch));
		camForward = glm::normalize(camForward);
		pCamera->SetForward(camForward);
		
	}
	if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
		pCamera->ProcessKeyboard(UP, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
		pCamera->ProcessKeyboard(DOWN, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS)
		pCamera->ProcessKeyboard(TEST, (float)deltaTime);


	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		pCamera->Reset(width, height);

	}
	
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	pCamera->Reshape(width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{

	pCamera->MouseControl((float)xpos, (float)ypos);
	camYaw = pCamera->GetYaw();
	camPitch = pCamera->GetPitch();
	//pCamera->MouseControlCircle((float)xpos, (float)ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yOffset)
{
	pCamera->ProcessMouseScroll((float)yOffset);
}